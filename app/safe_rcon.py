# coding=utf-8
import struct

import select

from app.rcon import SourceRcon, SERVERDATA_EXECCOMMAND, SERVERDATA_AUTH, \
    SourceRconError, MAX_COMMAND_LENGTH, MIN_MESSAGE_LENGTH, \
    MAX_MESSAGE_LENGTH, SERVERDATA_AUTH_RESPONSE, \
    PROBABLY_SPLIT_IF_LARGER_THAN, SERVERDATA_RESPONSE_VALUE

__author__ = "Gareth Coles"


class SafeRcon(SourceRcon):
    def rcon(self, command):
        """Send RCON command to the server. Connect and auth if necessary,
           handle dropped connections, send command and return reply."""
        # special treatment for sending whole scripts
        if '\n' in command:
            commands = command.split('\n')

            def f(x):
                y = x.strip()
                return len(y) and not y.startswith("//")

            commands = filter(f, commands)
            results = map(self.rcon, commands)
            return "".join(results)

        command = str.encode(command)

        # send a single command. connect and auth if necessary.
        try:
            self.send(SERVERDATA_EXECCOMMAND, command)
            return self.receive()
        except:
            # timeout? invalid? we don't care. try one more time.
            self.disconnect()
            self.connect()
            self.send(SERVERDATA_AUTH, self.password)

            auth = self.receive()
            # the first packet may be a "you have been banned" or empty string.
            # in the latter case, fetch the second packet
            if auth == '':
                auth = self.receive()

            if auth is not True:
                self.disconnect()
                raise SourceRconError('RCON authentication failure: %s' % (repr(auth),))

            self.send(SERVERDATA_EXECCOMMAND, command)
            return self.receive()

    def send(self, cmd, message):
        if len(message) > MAX_COMMAND_LENGTH:
            raise SourceRconError('RCON message too large to send')

        self.reqid += 1

        if isinstance(message, str):
            message = str.encode(message)

        data = (
            struct.pack('<l', self.reqid) +
            struct.pack('<l', cmd) +
            message +
            b'\x00\x00'
        )
        self.tcp.send(struct.pack('<l', len(data)) + data)

    def receive(self):
        """Receive a reply from the server. Should only be used internally."""
        size = False
        requestid = False
        response = False
        message = b''
        message2 = b''

        # response may be split into multiple packets, we don't know how many
        # so we loop until we decide to finish
        while 1:
            # read the size of this packet
            buf = b''

            while len(buf) < 4:
                try:
                    recv = self.tcp.recv(4 - len(buf))
                    if not len(recv):
                        raise SourceRconError(
                            'RCON connection unexpectedly closed by remote host'
                        )
                    buf += recv
                except SourceRconError:
                    raise
                except Exception as e:
                    break

            if len(buf) != 4:
                # we waited for a packet but there isn't anything
                print("No packet response available.")
                break

            size = struct.unpack('<l', buf)[0]

            if size < MIN_MESSAGE_LENGTH or size > MAX_MESSAGE_LENGTH:
                raise SourceRconError(
                    'RCON packet claims to have illegal size: %d bytes'
                    % size
                )

            # read the whole packet
            buf = b''

            while len(buf) < size:
                try:
                    recv = self.tcp.recv(size - len(buf))
                    if not len(recv):
                        raise SourceRconError(
                            'RCON connection unexpectedly closed by remote host'
                        )
                    buf += recv
                except SourceRconError:
                    raise
                except:
                    break

            if len(buf) != size:
                raise SourceRconError(
                    'Received RCON packet with bad length (%d of %d bytes)' % (
                        len(buf), size
                    )
                )

            # parse the packet
            requestid = struct.unpack('<l', buf[:4])[0]

            if requestid == -1:
                self.disconnect()
                raise SourceRconError('Bad RCON password')

            elif requestid != self.reqid:
                raise SourceRconError(
                    'RCON request id error: %d, expected %d' % (
                        requestid, self.reqid
                    )
                )

            response = struct.unpack('<l', buf[4:8])[0]

            if response == SERVERDATA_AUTH_RESPONSE:
                # This response says we're successfully authed.
                return True

            elif response != SERVERDATA_RESPONSE_VALUE:
                raise SourceRconError(
                    'Invalid RCON command response: %d' % response
                )

            # extract the two strings using index magic
            str1 = buf[8:]
            pos1 = str1.index(b'\x00')
            str2 = str1[pos1+1:]
            pos2 = str2.index(b'\x00')
            crap = str2[pos2+1:]

            if crap:
                raise SourceRconError(
                    'RCON response contains %d superfluous bytes' % len(crap)
                )

            # add the strings to the full message result
            message += str1[:pos1]
            message2 += str2[:pos2]

            # unconditionally poll for more packets
            poll = select.select([self.tcp], [], [], 0)

            if not len(poll[0]) and size < PROBABLY_SPLIT_IF_LARGER_THAN:
                # no packets waiting, previous packet wasn't large: let's stop
                break

        if response is False:
            raise SourceRconError('Timed out while waiting for reply')

        elif message2:
            raise SourceRconError(
                'Invalid response message: %s' % repr(message2)
            )

        return str(message, encoding="utf-8")
